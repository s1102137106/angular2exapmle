
import { MdDialogRef, MdDialog } from '@angular/material/dialog';
import { Component, OnInit, Input } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'dialog-result',
    template: '',
})

export class DialogResultComponent {
    selectedOption: string;

    constructor(public dialog: MdDialog) { this.openDialog() }

    openDialog() {
        let dialogRef = this.dialog.open(DialogComponent);
        dialogRef.afterClosed().subscribe(result => {
            this.selectedOption = result;
        });

    }
}


@Component({
    selector: 'dialog-context',
    templateUrl: './dialog.html',

})
export class DialogComponent {
    constructor(public dialogRef: MdDialogRef<DialogComponent>) { }
}
