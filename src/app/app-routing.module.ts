import { HomeComponent } from './home.component';
import { AppComponent } from './app.component';
import { OrderConditionComponent } from './Order/order-condition.component';
import { OrderResultComponent } from './Order/order-result.component';
import { OrderDetailComponent } from './Order/order-detail.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';



const routes: Routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: 'orderCondition', component: OrderConditionComponent },
    { path: 'orderDetail/:orderId', component: OrderDetailComponent },
    { path: 'home', component: HomeComponent },

];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }