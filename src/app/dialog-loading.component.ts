
import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'dialog-loading',
    templateUrl: './dialog-loading.html'
})
export class DialogLoadingComponent implements OnInit {
    @Output() closeDialog = new EventEmitter();
    @ViewChild('loadingDialog') loadingDialog;

    mdProgessCircle = { color: "warn", mode: "indeterminate", value: "70" }//操作讀取條

    constructor() { }

    ngAfterViewInit() {
        //畫面完成 才可使用 this.loadingDialog
        this.closeDialog.emit(this.loadingDialog)
    }

    ngOnInit() { }

}