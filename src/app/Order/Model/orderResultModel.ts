export class OrderResultModel {
    constructor(
        public OrderID: number,
        public CompanyName: string,
        public OrderDate: Date,
        public ShippedDate?: Date
    ) { }
}

