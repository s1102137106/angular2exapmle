export class ConditionModel {

  constructor(
    public orderDate:Date,
    public shipperDate:Date,
    public orderId?: number,
    public custName?:string,
    public empID?:string,
    public shipperID?:String
    
  ) {  }

}
