
export class Employee {

    constructor(
        public empid: string,
        public lastname: string,
        public firstname: string

    ) { }

}