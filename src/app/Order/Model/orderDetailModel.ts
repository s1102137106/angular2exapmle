
export class OrderDetailModel {
    constructor(
        public orderid: number,
        public productid: number,
        public unitprice: number,
        public qty: number,
        public discount: number,
    ) { }
}