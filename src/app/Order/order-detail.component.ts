import { OrderModel } from './Model/orderModel';
import { Shipper } from './Model/shipnameModel';
import { Employee } from './Model/employeeModel';
import { Dialog } from '../Interface/IDialog';
import { OrderDetailModel } from './Model/orderDetailModel';
import { SelectService } from './../Service/selectService';

import { Location } from '@angular/common';
import { Params, ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'detail',
    templateUrl: './order-detail.html'
})
export class OrderDetailComponent implements Dialog {
    constructor(private selectService: SelectService,
        private route: ActivatedRoute,
        private location: Location) { this.order = new OrderModel() }

    @ViewChild('DetailForm') detailForm;

    order: OrderModel;//訂單資料
    orderDetail: OrderDetailModel[];//訂單明細

    employeeList: Employee[];//Employee下拉式選單
    shipperList: Shipper[];//Shipper下拉式選單
    customers;//客戶下拉式選單
    products;//產品下拉式選單

    private errorMessage;
    loadingDialog;//loading畫面

    private title = '修改訂單'
    private subTitle = '訂單明細'


    private getEmployeeList() {
        this.openDialog();
        this.selectService.getSelectEmployee().subscribe(
            employeeList => this.employeeList = employeeList,
            error => this.errorMessage = <any>error,
            () => this.closeDialog());
    }

    private getSipperList() {
        this.openDialog();
        this.selectService.getSelectShipper().subscribe(
            shipperList => this.shipperList = shipperList,
            error => this.errorMessage = <any>error,
            () => this.closeDialog());
    }

    private getCustomers() {
        this.openDialog();
        this.selectService.getCustomers().subscribe(
            customers => this.customers = customers,
            error => this.errorMessage = <any>error,
            () => this.closeDialog());
    }

    private getProducts() {
        this.openDialog();
        this.selectService.getProducts().subscribe(
            products => this.products = products,
            error => this.errorMessage = <any>error,
            () => this.closeDialog());
    }



    ngAfterViewInit() {
        this.route.params
            .subscribe(params => this.getData(params));
        this.getEmployeeList();
        this.getSipperList();
        this.getCustomers();
        this.getProducts();
    }

    //得到頁面所需要的資源
    getData(params: Params) {
        this.openDialog();

        var orderId = +params['orderId'];
        this.selectService.getOrder(orderId).subscribe(
            order => this.order = order,
            error => this.errorMessage = <any>error,
            () => this.orderComplete());

        this.selectService.getOrderDetail(orderId).subscribe(
            orderDetail => this.orderDetail = orderDetail,
            error => this.errorMessage = <any>error,
            () => this.closeDialog());

    }
    public orderComplete() {
        this.order.orderdate = this.dateFormat(this.order.orderdate);
        this.order.shippeddate = this.dateFormat(this.order.shippeddate);
        this.order.requireddate = this.dateFormat(this.order.requireddate);
        this.closeDialog()
    }

    public dateFormat(date: string) {
        return date.toString().substring(0, 10);
    }

    public openDialog() {
        this.loadingDialog.show();
    }

    public closeDialog() {
        this.loadingDialog.hide();
    }

    //設定
    private setLoadingDialog(loadingDialog) {
        this.loadingDialog = loadingDialog
    }


    public onSubmit() {
        this.openDialog();
        this.selectService.putOrder(this.order.orderid, this.order).subscribe(
            () => this.closeDialog());

        this.selectService.putOrderDetails(this.orderDetail).subscribe(
            () => this.closeDialog());
    }

    get diagnostic() { return JSON.stringify(this.orderDetail); }
    get diaOrder() { return JSON.stringify(this.order); }

    get detailformdata() { return JSON.stringify(this.orderDetail) }




}