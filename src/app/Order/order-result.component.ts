import { OrderResultModel } from './Model/orderResultModel';
import { Input, Component, OnInit } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'order-result',
    templateUrl: 'order-result.html'
})
export class OrderResultComponent implements OnInit {
    constructor() { }
    @Input() orderResult: OrderResultModel;

    selectOrderId: number;

    deleteOrder(orderID: Number) {
    }

    get getOrderResult() { return JSON.stringify(this.orderResult); }

    ngOnInit() { }

}