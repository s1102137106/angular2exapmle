import { Employee } from './Model/employeeModel';
import { Dialog } from '../Interface/IDialog';
import { OrderResultModel } from './Model/orderResultModel';
import { ResultService } from './../Service/resultService';
import { NgForm } from '@angular/forms';
import { Shipper } from './Model/shipnameModel';
import { ConditionModel } from './Model/conditionModel';
import { SelectService } from './../Service/selectService';

import { ViewChild, Component, OnInit } from '@angular/core';

//查詢條件
@Component({
    moduleId: module.id,
    selector: 'order-condition',
    templateUrl: 'order-condition.html'
})

export class OrderConditionComponent implements OnInit, Dialog {
    constructor(private selectService: SelectService, private resultService: ResultService) { }
    title = '訂單搜尋';

    model: ConditionModel;//條件(要做查詢)
    errorMessage: string;//Service錯誤訊息
    employeeList: Employee[];//Employee下拉式選單
    shipperList: Shipper[];//Shipper下拉式選單
    orderResult: OrderResultModel[]//查詢後回傳結果
    loadingDialog;//loading畫面

    color = 'accent';
    mode = 'indeterminate'; 

    conditionForm: NgForm;
    @ViewChild('heroForm') currentConditionForm: NgForm;//在html宣告的都會在ViewChild queries 例如 #conditionForm="ngForm"

    ngOnInit() {
        this.model = new ConditionModel(null, null);
    }

    ngAfterViewInit() {
        this.getEmployeeList();
        this.getSipperList();
        this.onSubmit();//先初始Result
    }

    private getEmployeeList() {
        this.openDialog();
        this.selectService.getSelectEmployee().subscribe(
            employeeList => this.employeeList = employeeList,
            error => this.errorMessage = <any>error,
            () => this.closeDialog());
    }

    private getSipperList() {
        this.openDialog();
        this.selectService.getSelectShipper().subscribe(
            shipperList => this.shipperList = shipperList,
            error => this.errorMessage = <any>error,
            () => this.closeDialog());
    }

    private getOrderResult() {
        this.openDialog();
        this.resultService.getOrderResult(this.model).subscribe(
            res => this.orderResult = res,
            error => this.errorMessage = <any>error,
            () => this.closeDialog());
    }

    public openDialog() {
        this.loadingDialog.show();
    }

    public closeDialog() {
        this.loadingDialog.hide();
    }

    //設定
    private setLoadingDialog(loadingDialog) {
        this.loadingDialog = loadingDialog
    }

    get diagnostic() { return JSON.stringify(this.model); }

    onSubmit() {
        this.getOrderResult();
    };
}