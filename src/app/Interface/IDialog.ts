export declare abstract class Dialog {
    abstract openDialog(): void;
    abstract closeDialog(): void;
}