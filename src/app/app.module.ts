import { HomeComponent } from './home.component';
import {AppRoutingModule} from './app-routing.module';

import { OrderDetailComponent } from './Order/order-detail.component';
import { NgGridModule } from 'angular2-grid';

import { DialogLoadingComponent } from './dialog-loading.component';
import { DialogResultComponent, DialogComponent } from './dialog-result.component';

import { ResultService } from './Service/resultService';
import { OrderResultComponent } from './Order/order-result.component';
import { DataService } from './Service/dataService';

import { SelectService } from './Service/selectService';
import { OrderConditionComponent } from './Order/order-condition.component';

import { MaterialModule } from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import 'hammerjs';
import { AppComponent } from './app.component';

// RECOMMENDED (doesn't work with system.js)
import { ModalModule } from 'ng2-bootstrap/modal';

@NgModule({
  declarations: [
    AppComponent,
    OrderConditionComponent,
    OrderResultComponent,
    DialogResultComponent,
    DialogComponent,
    DialogLoadingComponent,
    OrderDetailComponent,
    HomeComponent
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    MaterialModule.forRoot(),
    ModalModule.forRoot(),
    NgGridModule,
    AppRoutingModule
  ],
  providers: [SelectService, DataService, ResultService],
  bootstrap: [AppComponent],
  entryComponents: [DialogComponent]
})
export class AppModule { }