import { Headers, Http, Response, JsonpModule } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class DataService {
    protected webApiUrl: string;//連結api的網址
    protected headers = new Headers({ 'Content-Type': 'application/json' });
    constructor(protected http: Http) { }

    protected getSelect(action: string): Observable<any[]> {
        return this.http.get(this.webApiUrl + action)
            .map(this.extractData)
            .catch(this.handleError);
    }

    protected getSingleSelect(action: string): Observable<any> {
        return this.http.get(this.webApiUrl + action)
            .map(this.extractData)
            .catch(this.handleError);
    }

    protected putData(action: string, data: any): Observable<Response> {
        return this.http.put(this.webApiUrl + action, JSON.stringify(data), { headers: this.headers })
    }

    protected postData(data: any, action): Observable<any> {
        return this.http
            .post(this.webApiUrl + action, JSON.stringify(data), { headers: this.headers })
            .map(this.extractData)
            .catch(this.handleError);
    }


    protected extractData(res: Response) {
        let body = res.json();
        if (body.value == undefined) {
            return body;
        }
        return body.value || {};
    }

    protected handleError(error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}