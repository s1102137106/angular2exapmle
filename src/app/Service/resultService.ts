import { OrderResultModel } from './../Order/Model/orderResultModel';
import { DataService } from './dataService';
import { Employee } from './../Order/Model/employeeModel';
import { Http, Response } from '@angular/http';

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ResultService extends DataService {

    constructor(protected http: Http) {
        super(http);
        this.webApiUrl = 'http://localhost:59674/api/';
    }

    protected extractData(res: Response) {
        let body = res.json();
        return body || {};
    }

    getOrderResult(data:any): Observable<OrderResultModel[]> {
        return this.postData(data,"Order")//抓取odata/Employee
    }


}