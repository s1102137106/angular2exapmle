import { OrderModel } from './../Order/Model/orderModel';

import { OrderDetailModel } from '../Order/Model/orderDetailModel';
import { DataService } from './dataService';
import { Employee } from './../Order/Model/employeeModel';
import { Http } from '@angular/http';

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class SelectService extends DataService {

    constructor(protected http: Http) {
        super(http);
        this.webApiUrl = 'http://localhost:59674/odata/';
    }

    public getSelectEmployee(): Observable<Employee[]> {
        return this.getSelect("Employees")//抓取odata/Employee
    }


    public getSelectShipper() {
        return this.getSelect("Shippers")//抓取odata/Shippers
    }

    public getOrderDetail(id: number): Observable<OrderDetailModel[]> {
        return this.getSelect("OdataOrder(" + id + ")/OrderDetails")//抓取odata/Shippers
    }

    public getOrder(id: number): Observable<OrderModel> {
        return this.getSingleSelect("OdataOrder(" + id + ")")//抓取odata/Shippers
    }

    public getCustomers(): Observable<any[]> {
        return this.getSingleSelect("Customers")//抓取odata/Shippers
    }

    public getProducts(): Observable<any[]> {
        return this.getSingleSelect("Products")//抓取odata/Shippers
    }

    public putOrderDetails(data:any) {
        this.webApiUrl = "http://localhost:59674/api/";
        var result = this.putData("Order",data)//抓取odata/Shippers
        this.webApiUrl = 'http://localhost:59674/odata/';
        return result;
    }


    public putOrder(id: number, data: any) {
        return this.putData("OdataOrder(" + id + ")", data)//抓取odata/Shippers
    }






}