import { AngularExpamlePage } from './app.po';

describe('angular-expamle App', function() {
  let page: AngularExpamlePage;

  beforeEach(() => {
    page = new AngularExpamlePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
